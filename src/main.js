import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import GameMenu from "./components/GameMenu.vue";
import GamePlay from "./components/GamePlay.vue";
import GameOver from "./components/GameOver.vue";
import vuetify from "./plugins/vuetify";

import Vuetify from "vuetify/lib";
// src/main.js

Vue.use(VueRouter);
Vue.use(Vuetify);

//Vue.use(vuetify)

Vue.config.productionTip = false;
const routes = [
  { path: "/", name: "Menu", component: GameMenu },
  { path: "/gameplay", name: "GamePlay", props: true, component: GamePlay },
  { path: "/gameover", name: "GameOver", props: true, component: GameOver }
];
const router = new VueRouter({ routes });

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount("#app");
